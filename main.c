#include <stdio.h>
#include <stdlib.h>
#include <math.h>

const double W=-44.02, R0=7.525,aRand=0.67;		/* Potential-Parameter */
const double rzero=1.27;				/* r0 für Relation von Dichte und A in fm*/

double lsfakt=0.44;				/* Definiert Relation von Uls und W */
const double hbarc2m=20.736;				/* hbarc**2/2m in MeV*fm^2 */ 


double determinant(unsigned int dimension, double lambda, double *diagonal, double *top_diagonal, double *bottom_diagonal)
{
	double D, D_m1, D_m2;

	D = diagonal[0] - lambda;
	D_m1 = D;
	D_m2 = 1;

	for(unsigned int i = 1; i < dimension; i++)
	{
		D = (diagonal[i] - lambda) * D_m1 - bottom_diagonal[i] * top_diagonal[i-1] * D_m2;
		#ifdef DEBUG_DETERMINANT
			printf("D: %lf\tD_m1: %lf\t diagonal: %lf\t lambda: %lf\t diagonal-lambda: %lf\tbottom_diag: %lf\ttop_diag: %lf\tbottom*top:%lf\n", D, D_m1, diagonal[i], lambda, diagonal[i] - lambda, bottom_diagonal[i], top_diagonal[i], bottom_diagonal[i]*top_diagonal[i-1]*D_m2);
		#endif
		D_m2 = D_m1;
		D_m1 = D;
	}
	return D;
}

static inline double V(double r, unsigned char l, char s) 
{	
	switch(s)
	{
		case 0:
			//Potential with no LS couppling
			return W/(1.0+exp((r-R0)/aRand));
		case 1:
			//Potential with LS couppling and s=1
			return W/(1.0+exp((r-R0)/aRand))
				+ (-lsfakt*W*(rzero*rzero))
				*0.5*l*(-0.1 / r * (0.1 + exp( (r - R0) / aRand ))/(0.2) / aRand * exp((r - R0) / aRand));
		
		case -1:
			//Potential with LS couppling and s=-1
			return W/(1.0+exp((r-R0)/aRand))
				-(-lsfakt*W*(rzero*rzero))
				*0.5*(l+1)*(-0.1 / r * (0.1 + exp((r - R0) / aRand))/(0.2) / aRand * exp((r - R0) / aRand));
		default:	
			abort();
	}
	return -1;
}
void init_DGL_x ( unsigned int steps, double step_size, double lower_boundary, double *x )
{
	/* Initialisiere Stuetzpunkte */ 
	for (unsigned int i = 0; i < steps; i++)
	{
		x[i] = lower_boundary + i * step_size;
	}
}

//void init_tridiagonal()
//{
//
//}

int main(int argc, char *argv[])
{
	const unsigned int steps = 50;
	char spin = 0;
	unsigned char l = 0;

	//CLI
//	char *options = "m:e:l:s:";
//
//	while( (opt = getopt(argc, argv, options)) != -1 )
//	{
//		switch(opt)
//		{
//			case 'm':
//				break;
//			case 'e':
//				break;
//			case 'l':
//				break;
//			case 's':
//				break;
//			case '?':
//				fprintf(stdout, "Usage:\n -m: Matrix size -> -m 5\n -e: Energy steps -> -m e 1000\n -l: Angular momentum {0,..,7}\n -s: Spin -> 0 - Choses simple potential without LS couppling.\n 1 - Choses LS couppling potential with s=1\n -1 - Choses LS couppling potential with s=-1.");
//				return -1;
//		}
//	}

	const double lower_boundary = 0.0;
	const double upper_boundary = 20.0;

	double step_size = (upper_boundary-lower_boundary)/steps;
	printf("step_size: %lf\n", step_size);
	double step_size_squared = step_size*step_size;

	//initialize values of DGL
	double *DGL_x = (double*) malloc(steps*sizeof(double));
	init_DGL_x(steps, step_size, lower_boundary, DGL_x);

	//initialize values for tridiagonals
	//a_0 = 0 and c_n = 0	
	double *alpha = (double*) malloc(steps*sizeof(double));
	double *beta  = (double*) malloc(steps*sizeof(double));
	double *gamma = (double*) malloc(steps*sizeof(double));

	alpha[0] = 0;
	
	//First element should be 1, if x=0 and used with the definition inside of the following for loop, this would create NaN values.
	beta[0]  = 1; 
	gamma[0] = -1;

	for(unsigned int i = 1; i < steps; i++)
	{
		alpha[i] = -1;
		beta[i]  = ( l * ( l + 1 ) / (DGL_x[i] * DGL_x[i]) - V(DGL_x[i], l, spin)/hbarc2m ) * step_size_squared + 2; 
		#ifdef DEBUG_BETA
			printf("beta[%u]: %lf\tDGL_x: %lf\tV: %lf\tstep_size_quared: %lf\n", i, beta[i], DGL_x[i], V(DGL_x[i], l, spin), step_size_squared);
		#endif
		gamma[i] = -1;
	}
	//Overwriting last element of gamma with 0.
	gamma[steps-1] = 0;

	//initializing energy and eigen values

	const double eigen_value_lower_boundary = 0.0;
	const double eigen_value_higher_boundary = 20.0;

	unsigned int energy_steps = 100;
	double energy_lower_boundary = eigen_value_lower_boundary * step_size_squared/hbarc2m;
	double energy_higher_boundary = eigen_value_higher_boundary * step_size_squared/hbarc2m;
	double energy_step_size = (energy_higher_boundary - energy_lower_boundary)/energy_steps;
	printf("energy_step_size: %lf\n", energy_step_size);


	//Generating Data
	FILE *fp;
	char file_path[124];
	snprintf(file_path, 124, "lndet-%d.tsv", l);
	if ( (fp = fopen(file_path, "w")) == NULL )
	{
		fprintf(stderr, "Something went wrong opening the file!");
		exit(-1);
	}
	for(double energy_current = 0.0; energy_current < energy_higher_boundary; energy_current += energy_step_size)
	{
		fprintf(fp, "%lf\t%lf\n", energy_current, log(abs(determinant(steps, energy_current, beta, gamma, alpha))));
		fprintf(stdout, "----------TEST EIGENVALUES---------\n\
			energy_current: %lf\tln(|det|): %lf\n", energy_current, log(abs(determinant(steps, energy_current, beta, gamma, alpha))));
	}

	free(DGL_x);

	free(alpha);
	free(beta);
	free(gamma);

	return 0;
}
