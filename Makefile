compile:
	gcc main.c -o main.o -lm -Wall -pedantic -fPIC -O3

debug_beta:
	gcc main.c -o main.o -lm -Wall -pedantic -fPIC -O3 -D DEBUG_BETA
debug_determinant:
	gcc main.c -o main -lm -Wall -pedantic -fPIC -O3 -D DEBUG_DETERMINANT
syntax:
	gcc main.c -fsyntax-only
